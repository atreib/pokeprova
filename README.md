# PokeProva

A PokeProva � uma prova realizada no dia 04/01/2018, no qual o participante deveria utilizar a API PokeApi (https://pokeapi.co/) e listar todos os tipos de Pokemon. A partir desta tela, o usu�rio deveria poder buscar todos os Pok�mons de um Tipo espec�fico e os dados deste Pok�mon. Na tela com os Dados do Pok�mon, deveria aparecer a foto do Pok�mon, nome, altura, peso e lista de habilidades.

Foi desenvolvivo usando o Android Studio (JAVA, aplicativo nativo).

# Bibliotecas Utilizadas

- Volley
- AndroidImageSlider (https://github.com/daimajia/AndroidImageSlider)