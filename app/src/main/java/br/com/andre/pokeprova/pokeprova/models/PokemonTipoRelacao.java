package br.com.andre.pokeprova.pokeprova.models;

/*
    Essa classe é utilizada para relacionar os pokemons POR TIPO de uma forma mais leve
        não necessitando da consulta direta à POKEMONS, após buscar os tipos
            (pois a consulta de TIPO já traz uma lista de Pokemons com as suas URLs)
*/

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class PokemonTipoRelacao {

    public String Nome;
    public String Url;

    // Função para converter o JSON em PokemonTipoRelacao
    public static ArrayList<PokemonTipoRelacao> ParsePokemons(JSONObject jsonTiposAux)
    {
        ArrayList<PokemonTipoRelacao> arrayRetorno = null; // array de retorno

        try {
            JSONArray jsonTipos = jsonTiposAux.getJSONArray("pokemon");

            // Se existe algo no JSON de retorno
            for (int i = 0; i < jsonTipos.length(); i++) {
                if (arrayRetorno == null) arrayRetorno = new ArrayList<PokemonTipoRelacao>();

                // Vamos pegar o Objeto JSON dos Pokemons
                JSONObject auxJson = jsonTipos.getJSONObject(i);
                JSONObject jsonPokemon = auxJson.getJSONObject("pokemon");

                // Montando nosso model de TIPO
                PokemonTipoRelacao poke = new PokemonTipoRelacao();
                poke.Nome = jsonPokemon.getString("name");
                poke.Url = jsonPokemon.getString("url");
                arrayRetorno.add(poke); // Retorna lista completa
            }
        } catch (Exception ex) {
            arrayRetorno = null; // Em caso de erro, retorna ao null
        }

        return arrayRetorno;
    }

}
