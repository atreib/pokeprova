package br.com.andre.pokeprova.pokeprova;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import br.com.andre.pokeprova.pokeprova.models.Pokemon;
import br.com.andre.pokeprova.pokeprova.models.PokemonTipoRelacao;

public class PokemonActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private SliderLayout mDemoSlider; // carrousel de fotos
    private ProgressBar spinner; // loading spinner

    // Dados do TIPO do Pokemon respectivo
    public String urlTipo;
    public String nomeTipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        // Inicia o Spinner
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);

        // Recebendo valores da view anterior
        Intent intent = getIntent();
        String urlPoke = intent.getStringExtra("urlPoke");
        String nomePoke = intent.getStringExtra("pokemon");
        urlTipo = intent.getStringExtra("urlTipo");
        nomeTipo = intent.getStringExtra("nomeTipo");
        setTitle("Pokemon: " + nomePoke + " (tipo '" + nomeTipo + "')"); // seta title da view

        // Buscando dados do Pokemon
        BuscaPokemon(this, urlPoke);
    }

    @Override
    public void onBackPressed() { // Tratando botão de voltar
        Intent intent = new Intent(this, ListaPokemonsActivity.class);
        intent.putExtra("url", urlTipo);
        intent.putExtra("tipo", nomeTipo);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // Tratando botão de voltar do StatusBar
                Intent intent = new Intent(this, ListaPokemonsActivity.class);
                intent.putExtra("url", urlTipo);
                intent.putExtra("tipo", nomeTipo);
                startActivity(intent); // Abre Lista de Pokemons deste tipo
                break;
        }
        return true;
    }

    // Buscando dados do Pokemon 
    public void BuscaPokemon(final PokemonActivity contexto, String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(contexto);

        // GET Request
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Convertendo JSON para Model
                        final Pokemon pokemon = Pokemon.ParsePokemon(response);

                        // Criando Slider de Fotos
                        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
                        HashMap<String,String> url_maps = new HashMap<String, String>();
                        for (int i = 0; i < pokemon.Fotos.size(); i++) {
                            url_maps.put(pokemon.Fotos.get(i), pokemon.Fotos.get(i));
                        }
                        for(String name : url_maps.keySet()){
                            TextSliderView textSliderView = new TextSliderView(PokemonActivity.this);
                            // inicializar SliderLayout
                            textSliderView
                                    .image(url_maps.get(name))
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(PokemonActivity.this);
                            mDemoSlider.addSlider(textSliderView);
                        }
                        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
                        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                        mDemoSlider.setDuration(4000);
                        mDemoSlider.addOnPageChangeListener(PokemonActivity.this);

                        // Preechendo informações do pokemon
                        TextView txtNome = (TextView)findViewById(R.id.nome);
                        txtNome.setText(pokemon.Nome);
                        TextView txtAltura = (TextView)findViewById(R.id.altura);
                        txtAltura.setText((pokemon.Altura * 30.48) + "cm de Altura");
                        TextView txtPeso = (TextView)findViewById(R.id.peso);
                        txtPeso.setText(pokemon.Peso + "kg");

                        // Preenchendo Habilidades
                        ArrayAdapter<String> itemsAdapter =
                                new ArrayAdapter<String>(PokemonActivity.this, android.R.layout.simple_list_item_1, pokemon.Habilidades);
                        ListView listView = (ListView) findViewById(R.id.listHabilidades);
                        listView.setAdapter(itemsAdapter);

                        // Criando botão para compartilhar
                        Button btnShare = (Button) findViewById(R.id.share);
                        btnShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                String texto = "O " + pokemon.Nome + ", do tipo " + nomeTipo + ", tem " + (pokemon.Altura * 30.48) + "cm de altura e pesa " + pokemon.Peso + "kgs.";
                                sendIntent.putExtra(Intent.EXTRA_TEXT, texto);
                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                            }
                        });

                        // Encerrando Spinner
                        spinner.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Em caso de erro, encerra o Spinner
                        spinner.setVisibility(View.GONE);
                    }
                });

        requestQueue.add(jsonObjectRequest);
    }

    /*
        Funções do Carrousel
     */

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {}

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {}

    @Override
    public void onPageScrollStateChanged(int state) {}
}
