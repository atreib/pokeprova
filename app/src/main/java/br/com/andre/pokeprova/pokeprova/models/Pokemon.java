package br.com.andre.pokeprova.pokeprova.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Pokemon {

    public String Nome;
    public int Altura;
    public int Peso;
    public ArrayList<String> Habilidades;
    public ArrayList<String> Fotos;

    // Função para converter o JSON em Pokemon
    public static Pokemon ParsePokemon(JSONObject jsonPokemon)
    {
        Pokemon retorno = null; // retorno

        try {
            if (retorno == null) retorno = new Pokemon();

            // Buscando habilidade do pokemon no JSON
            JSONArray arrayHabilidades = jsonPokemon.getJSONArray("abilities");
            for (int i = 0; i < arrayHabilidades.length(); i++) {
                JSONObject jsonHabilidade = arrayHabilidades.getJSONObject(i);
                if (retorno.Habilidades == null) retorno.Habilidades = new ArrayList<String>();

                JSONObject habilidade = jsonHabilidade.getJSONObject("ability");
                retorno.Habilidades.add(habilidade.getString("name"));
            }

            // Buscando fotos do pokemon no JSON
            JSONObject jsonFotos = jsonPokemon.getJSONObject("sprites");
            retorno.Fotos = new ArrayList<String>();
            String aux = jsonFotos.getString("front_shiny_female");
            if (aux != null && aux != "null")
                retorno.Fotos.add(aux);
            aux = jsonFotos.getString("back_default");
            if (aux != null && aux != "null")
                retorno.Fotos.add(aux);
            aux = jsonFotos.getString("back_female");
            if (aux != null && aux != "null")
                retorno.Fotos.add(aux);
            aux = jsonFotos.getString("back_shiny");
            if (aux != null && aux != "null")
                retorno.Fotos.add(aux);
            aux = jsonFotos.getString("back_shiny_female");
            if (aux != null && aux != "null")
                retorno.Fotos.add(aux);
            aux = jsonFotos.getString("front_default");
            if (aux != null && aux != "null")
                retorno.Fotos.add(aux);
            aux = jsonFotos.getString("front_female");
            if (aux != null && aux != "null")
                retorno.Fotos.add(aux);
            aux = jsonFotos.getString("front_shiny");
            if (aux != null && aux != "null")
                retorno.Fotos.add(aux);

            // Montando nosso model de POKEMON
            retorno.Nome = jsonPokemon.getString("name");
            retorno.Altura = jsonPokemon.getInt("height");
            retorno.Peso = jsonPokemon.getInt("weight");
        } catch (Exception ex) {
            retorno = null; // Em caso de erro, retorna ao null
        }

        return retorno;
    }

}
