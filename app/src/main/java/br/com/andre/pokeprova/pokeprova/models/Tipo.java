package br.com.andre.pokeprova.pokeprova.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class Tipo {

    public String Nome;
    public String Url;

    // Função para converter o JSON em Tipo
    public static ArrayList<Tipo> ParseTipos(JSONObject jsonTiposAux)
    {
        ArrayList<Tipo> arrayRetorno = null; // array de retorno

        try {
            JSONArray jsonTipos = jsonTiposAux.getJSONArray("results");

            // Se existe algo no JSON de retorno
            for (int i = 0; i < jsonTipos.length(); i++) {
                if (arrayRetorno == null) arrayRetorno = new ArrayList<Tipo>();

                // Vamos pegar o Objeto JSON do Tipo
                JSONObject jsonTipo = jsonTipos.getJSONObject(i);

                // Montando nosso model de TIPO
                Tipo tipo = new Tipo();
                tipo.Nome = jsonTipo.getString("name");
                tipo.Url = jsonTipo.getString("url");
                arrayRetorno.add(tipo); // Retorna lista completa
            }
        } catch (Exception ex) {
            arrayRetorno = null; // Em caso de erro, retorna ao null
        }

        return arrayRetorno;
    }

}
