package br.com.andre.pokeprova.pokeprova;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.andre.pokeprova.pokeprova.models.PokemonTipoRelacao;
import br.com.andre.pokeprova.pokeprova.models.Tipo;

public class ListaPokemonsActivity extends AppCompatActivity {

    private ProgressBar spinner; // loading spinner da view

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pokemons);

        // Rolando o spinner
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);

        // Recebendo valores da view anterior
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        if (url != null) {
            String nomeTipo = intent.getStringExtra("tipo");
            setTitle("Pokemons (" + nomeTipo + ")"); // Seta o titulo da View

            BuscaPokemons(this, url, nomeTipo); // Buscando pokemons deste tipo
        } else {
            spinner.setVisibility(View.GONE); // Caso não venha nada no Intent, desliga o spinner
        }
    }

    @Override
    public void onBackPressed() { // Lidando com o botão de voltar do ANDROID
        Intent intent = new Intent(this, TiposActivity.class);
        startActivity(intent);
    }

    // Buscando Todos Pokemons do TIPO SELECIONADO
    public void BuscaPokemons(final ListaPokemonsActivity contexto, final String urlTipo, final String nomeTipo) {
        RequestQueue requestQueue = Volley.newRequestQueue(contexto);

        // GET REQUEST - JSON
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, urlTipo, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Convertendo JSON para Model
                        final ArrayList<PokemonTipoRelacao> arrayPokemons = PokemonTipoRelacao.ParsePokemons(response);

                        // Criando adapter para MODEL to VIEW
                        ArrayAdapter<PokemonTipoRelacao> arrayAdapter = new ListaPokemonsActivity.PokemonsArrayAdapter(
                                contexto,
                                0,
                                arrayPokemons);

                        // Pegando ListView no XML
                        ListView lv = (ListView) findViewById(R.id.listViewPokemons);

                        // Populando a ListView
                        lv.setAdapter(arrayAdapter);

                        // Criando função de selecionar um POKEMON
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                                PokemonTipoRelacao poke = arrayPokemons.get(position);
                                String urlPoke = poke.Url;

                                // Abrindo tela do pokemon
                                Intent intent = new Intent(contexto, PokemonActivity.class);
                                intent.putExtra("nomeTipo", nomeTipo);
                                intent.putExtra("urlTipo", urlTipo);
                                intent.putExtra("urlPoke", urlPoke);
                                intent.putExtra("pokemon", poke.Nome);
                                startActivity(intent);
                            }
                        });

                        // Encerra o Spinner
                        spinner.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    // Em caso de erro, desliga o spinner
                    public void onErrorResponse(VolleyError error) { spinner.setVisibility(View.GONE); }
                });

        requestQueue.add(jsonObjectRequest);
    }

    // Adapter do ListView para/com model PokemonTipoRelacao
    class PokemonsArrayAdapter extends ArrayAdapter<PokemonTipoRelacao> {

        private Context context;
        private List<PokemonTipoRelacao> baseDados;

        //constructor, call on creation
        public PokemonsArrayAdapter(Context context, int resource, ArrayList<PokemonTipoRelacao> objects) {
            super(context, resource, objects);
            this.context = context;
            this.baseDados = objects;
        }

        // tratando click em item da lista
        public PokemonTipoRelacao getItem(int position){
            return baseDados.get(position);
        }

        //called when rendering the list
        public View getView(int position, View convertView, ViewGroup parent) {

            //get the property we are displaying
            PokemonTipoRelacao poke = baseDados.get(position);

            // get the inflater and inflate the XML layout for each item
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.lista_pokemons, null);

            // Setando valor dos campos do XML
            TextView nome = (TextView) view.findViewById(R.id.nome);
            nome.setText(poke.Nome);

            TextView url = (TextView) view.findViewById(R.id.url);
            url.setText(poke.Url);

            return view;
        }
    }
}
