package br.com.andre.pokeprova.pokeprova;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import br.com.andre.pokeprova.pokeprova.models.Tipo;

public class TiposActivity extends AppCompatActivity {

    private ProgressBar spinner; // Loading spinner da VIEW

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipos);

        // Iniciando loading
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);

        // Buscando lista de tipos de pokemons
        BuscaTipos(this);
    }

    // Buscar todos os tipos de pokemon
    public void BuscaTipos(final TiposActivity contexto) {
        // URL da API
        String url = "https://pokeapi.co/api/v2" + "/type/";

        // Criadno queue de requests desta view
        RequestQueue requestQueue = Volley.newRequestQueue(contexto);

        // GET Request
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Convertendo JSON para Model
                        final ArrayList<Tipo> arrayTipos = Tipo.ParseTipos(response);

                        // Criando adapter para MODEL to VIEW
                        ArrayAdapter<Tipo> arrayAdapter = new TiposArrayAdapter(
                                contexto,
                                0,
                                arrayTipos);

                        // Pegando ListView no XML
                        ListView lv = (ListView) findViewById(R.id.listViewTipos);

                        // Populando a ListView
                        lv.setAdapter(arrayAdapter);

                        // Criando função de selecionar um TIPO
                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                                // Capturando qual TIPO o user clicou
                                Tipo tipo = arrayTipos.get(position);
                                String url = tipo.Url; // URL da API

                                // Abrindo tela com lista de pokemons deste tipo
                                Intent intent = new Intent(contexto, ListaPokemonsActivity.class);
                                intent.putExtra("url", url);
                                intent.putExtra("tipo", tipo.Nome);
                                startActivity(intent);
                            }
                        });

                        // Encerra o loading
                        spinner.setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) { 
                        // Em caso de erro, encerra o loading
                        spinner.setVisibility(View.GONE); 
                    }
                });

        // Adicionando request à queue
        requestQueue.add(jsonObjectRequest);
    }

    // Adapter da ListView para/com o model Tipo
    class TiposArrayAdapter extends ArrayAdapter<Tipo>{

        private Context context;
        private List<Tipo> baseDados;

        //constructor, call on creation
        public TiposArrayAdapter(Context context, int resource, ArrayList<Tipo> objects) {
            super(context, resource, objects);
            this.context = context;
            this.baseDados = objects;
        }

        // tratando click em item da lista
        public Tipo getItem(int position){
            return baseDados.get(position);
        }

        //called when rendering the list
        public View getView(int position, View convertView, ViewGroup parent) {

            //get the property we are displaying
            Tipo tipo = baseDados.get(position);

            // get the inflater and inflate the XML layout for each item
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.lista_tipo, null);

            // Setando valor dos campos do XML
            TextView nome = (TextView) view.findViewById(R.id.nome);
            nome.setText(tipo.Nome);

            TextView url = (TextView) view.findViewById(R.id.url);
            url.setText(tipo.Url);

            return view;
        }
    }
}
